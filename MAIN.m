clear all
close all
clc

%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%% +++++++++++++++I N I Z I A L I Z Z A Z I O N E+++++++++++++++++++++++++++++++
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

% -------------------------------------------------- CASE ----------------------------------------------------------------
CASE=2;
% CASE = 1         \lambda_ref = 0.12;
% CASE=2         \lambda_ref = 0.45; 

% -------------------------------------------------- PLOT ----------------------------------------------------------------
PLOT=1;
% Se PLOT=1 vengono mostrati i plot

% -------------------------------------------- INIZIALIZATION --------------------------------------------------------
% Initialization script
Initialization

% Forzante
time=linspace(0,0.3,1000);
Fc_simulink(:,1) = time';
 omega=0;
Fc_simulink(:,2) =(5e0*sin(omega*time)+0*ones(size(time)))'; % Forzante nulla


%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%% ++++++++++++++++++A N E L L O   A P E R T O++++++++++++++++++++++++++++++
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

%% +++++++++++++++++++++Moto libero++++++++++++++++++++++++++++++++++++

sys=sim('SimulinkScheme_AnelloAperto.slx');

%-------------------------------Roba per garfico----------------------------------------------------------------------
T_end=sys.lambda.time(end);

tempo=sys.lambda.time;
lambda=sys.lambda.Data;
y_a=ref*ones(size(time));

%--------------------------------------------------------------------------------------------------------------------------

% PLOT RISPOSTA LIBERA
if PLOT==1 
    figure
    hold all
    plot(tempo,lambda,'linewidth',2)
    plot(tempo,ref*ones(length(tempo),1),'linewidth',2,'linestyle','--')
    xlabel('tempo [s]')
    ylabel('\lambda [-]')
    grid on
    legend('\lambda_{(t)}','riferimento')
    title('Risposta libera')
end

%% ++++++++++++++++++Linearizzazione anello aperto+++++++++++++++++++++++++
%Timed-based linearizzation block
AL = SimulinkScheme_AnelloAperto_Timed_Based_Linearization.a;
BL = SimulinkScheme_AnelloAperto_Timed_Based_Linearization.b;
CL = SimulinkScheme_AnelloAperto_Timed_Based_Linearization.c;
DL = SimulinkScheme_AnelloAperto_Timed_Based_Linearization.d;

% f = errordlg('Perché non ha senso la linearizzazione???????','Linearization Error');

% xo=[ref, 0];
% [AL,BL,CL,DL]=linmod('SimulinkScheme_AnelloAperto' , xo, 0);

sys_lin = ss(AL(1,1),BL(1,1),CL(1,1),DL(1,1)); % spazio di stato

% Risposta 
risposta_linearizzata =  lsim(sys_lin,Fc_simulink(:,2),time);
lambda_linearizzato    =  risposta_linearizzata(:,1); % Prima uscita (porta output numero 1)

%% ++++++++++++Funzione di trasferimento in anello aperto++++++++++++++++++++++
G_tf=tf(sys_lin);
G_tf=G_tf(1);
TransferFunction(G_tf,tempo,ref,PLOT)

if PLOT==1
    subplot(2,2,4)
    plot(tempo,lambda,'linewidth',2)
    hold all
    plot(time,(lambda_linearizzato),'linewidth',2)
    xlabel('tempo [s]')
    ylabel('\lambda [-]')
    grid on
    legend('\lambda','\lambda_{lin}','Location','best')
    title('\lambda_{(t)}')
    hold off
end
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%% ++++++++++++++++++++C O N T R O L L O R E++++++++++++++++++++++++++++++
%% ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
switch CASE
    case 1
        Num=1e6;
        Den=[1 1];
    case 2
        Num=1e6;
        Den=[0.1 10];
end

% Reg=pidtune(G_tf,'P');

Reg=tf(Num,Den);
L=Reg*G_tf;

TransferFunction(L,tempo,ref,PLOT)

% Simulazione
% NOTA: abbiamo impostato il file simulink con un solutore a passo fisso dt
% da tempo(1) a tempo(end)
out=sim("SimulinkScheme_Main");% Simulazione
% -------------------------------Roba per garfico----------------------------------------------------------------------
T_end=sys.lambda.time(end);

tempo=out.lambda.time;
lambda=out.lambda.Data;
%--------------------------------------------------------------------------------------------------------------------------

% PLOT RISPOSTA LIBERA
[S, Ta1]=parameters(lambda, tempo, ref, 1,PLOT,1);


% filtro sul riferimento
% pidTuner
% wind up
%controlSystemDesigner

function [S, Ta_eps,f]=parameters(y, t, yinf, eps,PLOT,SUBPLOT)
%  [S, Ta]=parameters(y,t,ref)
% 
% INPUT:
% y: vettore delle uscite
% t: vettore tempo
% yinf: valore asintotico
% eps: tempo di assestamento a ∓ε
%PLOT: boolean plot option
%
% OUTPUT
% S: sovraelongazione massima precentuale
% Ta: temo di assentsamento

% Sovraelongazione massima
S=100*(max(y)-yinf)/yinf;

%Tempo di assestamento
ind=find(abs(y-yinf)<=0.01*eps);

start=length(ind)-1;
for i=start:-1:1
    if (ind(i+1)-ind(i)==1)
        t_ind=ind(i);
    else
        break
    end
end

Ta_eps=t(t_ind);

ymax=yinf+0.01*eps;
ymin=yinf-0.01*eps;

if PLOT==1 

    if nargin==6
        if SUBPLOT==1
            subplot(2,2,4)
        else
            figure
        end
    end
    
    hold all
    plot(t,y,'linewidth',2)
    plot(t,yinf*ones(length(t),1),'linewidth',2,'linestyle','--')
    plot(t,ymax*ones(length(t),1),'linewidth',2,'linestyle',':','Color','k')
    plot(t,ymin*ones(length(t),1),'linewidth',2,'linestyle',':','Color','k')

%     Settings
    xlabel('tempo [s]')
    ylabel('\lambda [-]')
    
    legend('\lambda_{(t)}','riferimento')

    title_str=strcat('Risposta libera: Ta= ', num2str(Ta_eps), 's  S%= ', num2str(S), '%');
    title(title_str),

     grid on
end

end

function []=TransferFunction(Gtf,t,yinf,PLOT)
% []=TransferFunction(Gtf,t,yinf,PLOT)
% 
% INPUT
% Gtf: 1x1 tf
% t: time vector
% yinf: valore asintotico
% PLOT: boolean plot option
% 
% OUTPUT
% Null

if PLOT==1
    figure
    subplot(2,2,1)
    margin(Gtf)
    grid on
    
    subplot(2,2,2)
    pzmap(Gtf)
    grid on

    subplot(2,2,3)
    step(Gtf)
    hold on
    plot(t, yinf*ones(length(t),1), 'r--' )
end
end
