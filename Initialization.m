%Single Corner Model of car for Triplo anello

%% Data from table 2.2 - Parameters of the single-corner model,
%    from book 'Active Braking control systems design' by S.M. Savaresi and M. Tanelli
radius = 0.3; % Wheel radius
J = 1; % Wheel moment of inertia
m = 225; % Quarter car mass
v_init = 20; % Initial speed [m/s]
% Burckhardt model for longitudinal slip coefficient
% Parameters for dry asphalt  23.99 0.52
th_r1 = 1.28;
th_r2 = 23.99;
th_r3 = 0.52;

if PLOT==1 
    %Plot friction characteristics
    lambda = 0:0.01:1;
    mu = th_r1*(1-exp(-lambda*th_r2))-lambda*th_r3;
    figure(1)
    plot(lambda,mu);
    title('');
    grid on
    xlabel('Longitudinal Slip \lambda')
    ylabel('Longitudinal friction coeficient \mu');
end
%% Quarter car model
% Second order model with state variables (\lambda,v)
% Assumption (no load transfer): Fz = m*g
g = 9.81;
Fz = m*g;
lambda0 = 0;

% Valori da inseguire
switch CASE
    case 1
        ref = 0.12;
    case 2
        ref = 0.45; 
end