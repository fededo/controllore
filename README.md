# PROGETTO DEL CONTROLLORE #

## Controllo della dinamica longitudinale, principalmente influenzata da trazione e frenata ##

Uno dei problemi principali della dinamica longitudinale è lo slittamento (skidding) definito 
come ‘’unwanted sideways movement [of an automotive vehicle] not planned by the driver
causing a total loss of control of the vehicle by the driver.‘’

Progettare due controllori e simularli per i seguenti riferimenti: 

 1. 𝜆= 0.12

 2. 𝜆= 0.45
 
L’obiettivo dei controllori progettati è quello di ottenere i migliori risultati possibili 
minimizzando: 

o Sovraelongazione massima 𝑺% 

o Tempo di assestamento 𝑻𝒂

sull’uscita misurata 𝜆 garantendo al contempo errore nullo a transitorio esaurito. 


#### Richieste Progetto Finale ####

Valutazione Performance schema di controllo 

Gli obiettivi di controllo ‘‘ottimi’’ (ovvero la minimizzazione di 𝑺% e 𝑻𝒂 , garantendo errore nullo a
transitorio esaurito) saranno valutati sul modello nonlineare. 

Sono richiesti, per ogni problema di controllo: 

o Plot della 𝑣𝑎𝑟𝑖𝑎𝑏𝑖𝑙𝑒 𝑑𝑖 𝑢𝑠𝑐𝑖𝑡𝑎 𝜆 in funzione del tempo 

o Plot della 𝑣𝑎𝑟𝑖𝑎𝑏𝑖𝑙𝑒 𝑑𝑖 𝑐𝑜𝑛𝑡𝑟𝑜𝑙𝑙𝑜 𝑇𝑏 in funzione del tempo 

relativi alle simulazioni eseguite con il modello nonlineare controllato.


## ToDo ##
- [x] Anello aperto
- [x] Linearizzazione
- [ ] Funzione di trasferimento
- [ ] Controllore
- [ ] Wind-up
- [ ] Analisi in anello chiuso
- [ ] Sistema non lineare
- [ ] Consegna :tada:


## Come usare git: ##
```
1. pull
2. modify the project
3. save
4. commit
5. push
```

## Files ##

- ***Initialization***:
File di inizializzazione dei parametri.

- ***MAIN***:
File di controllo dei parametri.

-  ***Quarter_car_model***:
Sistema differenziale quarter car.

- ***SimulinkScheme_Main***:
Sistema su anello chiuso, su cui andranno valutati i parametri richiesti.

- ***SimulinkScheme_AnelloAperto***:
Sistema in anello aperto.



Per maggiori informazioni su come modificare il file Readme.md vedere al seguente [sito](https://docs.github.com/en/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax).